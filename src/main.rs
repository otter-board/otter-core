extern crate systemstat;
extern crate serde;
mod unix_server;
mod system_observer;

use std::env;
use std::path::Path;
use std::fs;

fn main() {

    //system_observer::example();

    let args: Vec<String> = env::args().collect();
    let socket_path = if args.len() == 2 { args[1].as_str() } else { "/tmp/otter_socket" };

    if Path::new(socket_path).exists() {
        fs::remove_file(socket_path).expect("Unable to delete old socket");
    }

    let server_handle = match unix_server::start_server(socket_path) {
        Ok(handle) => handle,
        Err(e) => {
            println!("Couldn't start the server : {}", e);
            return
        }
    };

    server_handle.join().expect("Enable to join server handle");
}

