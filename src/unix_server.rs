use std::os::unix::net::{UnixListener, UnixStream};
use std::net::Shutdown;
use std::io::{self, Read, Write, BufReader, BufWriter, BufRead};
use std::thread::{self, JoinHandle};
use serde::{Serialize, Deserialize};
use serde_json;

const COMMUNICATION_BUFFER_SIZE: usize = 2048;

pub fn start_server(path: &str) -> Result<JoinHandle<()>, io::Error> {
    match UnixListener::bind(path) {
        Ok(listener) => {
            println!("New socket created, local address is {:?}",
                     listener.local_addr().expect("Unable to get the local address"));
            Ok(thread::spawn(|| listen_connections(listener)))
        },
        Err(e) => Err(e)
    }
}

fn listen_connections(listener: UnixListener) {
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("New connection");
                thread::spawn(|| handle_client(stream));
            }
            Err(e) => {
                println!("Failed to accept new connection : {}", e);
            }
        }
    }
}

fn handle_client(connection_stream: UnixStream) {
    let mut keep_alive = true;

    let mut reader = BufReader::new(match connection_stream.try_clone() {
        Ok(stream) => { stream }
        Err(e) => {
            println!("Unable to duplicate the connection stream : {}", e);
            return
        }
    });
    let mut writer = BufWriter::new(match connection_stream.try_clone() {
        Ok(stream) => { stream }
        Err(e) => {
            println!("Unable to duplicate the connection stream : {}", e);
            return
        }
    });

    while keep_alive {
        let mut read_buffer = [0 as u8; COMMUNICATION_BUFFER_SIZE];
        let mut write_buffer = [0 as u8; COMMUNICATION_BUFFER_SIZE];

        match reader.read(&mut read_buffer) {
            Ok(size) => {
                if size == 0 {
                    println!("Closing connection");
                    keep_alive = false;
                } else {
                    println!("Buffer size : {}", size);
                    handle_response(&read_buffer);
                }
            }
            Err(e) => {
                println!("Enable to get the response : {}", e);
                keep_alive = false;
            }
        }
    }
}

fn handle_response(buffer: &[u8]) {
    let response = buffer_to_string(buffer);
    let message = Message::from_json(response);
    println!("{:?}", message);
}

fn buffer_to_string(buffer: &[u8]) -> String {
    let response = match String::from_utf8(Vec::from(&buffer[..])) {
        Ok(response) => { response }
        Err(e) => {
            println!("Error parsing data : {}", e);
            String::new()
        }
    };
    String::from(response.trim_end_matches(char::from(0)))
}

#[derive(Debug)]
enum Message {
    Get(IdMessage),
    Set(BindingMessage),
    Response(BindingMessage),
    Error(BindingMessage),
    Notify(BindingMessage),
    Update(BindingMessage),
    Stop(IdMessage)
}

impl Message {
    fn from_json(json: String) -> Message {
        let message: IdMessage = serde_json::from_str(json.as_str()).unwrap();
        let message = Message::Get(message);
        message
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct Binding {
    id: String,
    value: String
}

#[derive(Serialize, Deserialize, Debug)]
struct IdMessage {
    message: String,
    timestamp: String,
    fragmented: bool,
    offset: u32,
    ids: Vec<String>
}

#[derive(Serialize, Deserialize, Debug)]
struct BindingMessage {
    message: String,
    timestamp: String,
    fragmented: bool,
    offset: u32,
    binding: Vec<Binding>
}